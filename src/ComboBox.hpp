class ComboBox : public QComboBox {

    public:

        void keyReleaseEvent(QKeyEvent *e)
        {
            if(e->key() == Qt::Key_Escape)
                this->clearFocus();
        }

};
