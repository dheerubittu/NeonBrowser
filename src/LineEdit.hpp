class LineEdit : public QLineEdit
{
    public:
        QStringList HIST, TEMP;
        int pos = -1;
        LineEdit() {
        }
        ~LineEdit() {}
        
        void keyReleaseEvent(QKeyEvent *e)
        {
            if(e->key() == Qt::Key_Escape)
                this->clearFocus();
            else if(e->key() == Qt::Key_Up)
            {
                pos++;
                qDebug() << HIST;
            }
        }
        void keyPressEvent(QKeyEvent *e)
        {
            TEMP << e->text();
            if(e->key() == Qt::Key_Enter)
            {
                qDebug() << TEMP;
                HIST << TEMP;
                TEMP.clear();
                pos = -1;
            }
            QLineEdit::keyPressEvent(e);
        }

        void focusInEvent(QFocusEvent *e)
        {
            this->selectAll();
        }
};
