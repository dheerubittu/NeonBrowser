#include "MainWindow.hpp"
#include "Settings.cpp"

#define Shortcut(key, parent) QShortcut(QKeySequence(key), parent)

MainWindow :: MainWindow()
{
    OPTS << "open" << "reload" << "forward" << "back" << "find" << "quit";
    init_ui();
    init_settings();
    init_shortcuts();
    init_stylesheets();
    addNewTab(ns->value("Profile/StartPage", "about:blank").toString());
}

void MainWindow::init_stylesheets()
{
    QString urlBoxBg = "#000000",
            urlBoxFg = "#D4D4D4",
            cmdBoxBg = "#1e3440",
            urlBoxFont = ns->value("Font/CommandBox", "Roboto").toString(),
            urlBoxFontSize = ns->value("Font/CommandBoxSize", "Roboto").toString(),
            urlBoxUnfocusedBg = ns->value("Color/CommandBoxUnFocused/BG", "#222222").toString(),
            urlBoxUnfocusedFg = ns->value("Color/CommandBoxUnFocused/FG", "#444444").toString(),
            urlPlaceHolderFg = ns->value("Color/CommandBoxPlaceHolder/FG", "#222222").toString(),
            urlPlaceHolderBg = ns->value("Color/CommandBoxPlaceHolder/BG", "#222222").toString(),
            modeBg = urlBoxBg,
            modeFg = urlBoxFg,
            urlTextFg = ns->value("Color/URLText/FG", "#FF5000").toString(),
            urlTextBg = ns->value("Color/URLText/BG", "#1e3440").toString(),
            tabBg = "#222222",
            tabFg = "#777777",
            tabBgSelected = ns->value("Color/TabSelected/BG", "#1e3440").toString(),
            tabFgSelected = ns->value("Color/TabSelected/FG", "#D4D4D4").toString(),
            tabBgUnselected = ns->value("Color/TabUnSelected/BG", "#000000").toString(),
            tabFgUnselected = ns->value("Color/TabUnSelected/FG", "#333333").toString(),
            tabFgHover = ns->value("Color/TabHover/FG", "#444444").toString(),
            tabBgHover = ns->value("Color/TabHover/BG", "#444444").toString(),
            tabSelectedFont = ns->value("Font/TabSelected", "Roboto").toString(),
            tabUnSelectedFont = ns->value("Font/TabUnSelected", "Roboto").toString(),
            tabSelectedSize = ns->value("Font/TabSelectedSize", "12px").toString(),
            tabUnSelectedSize = ns->value("Font/TabUnSelectedSize", "12px").toString(),
            tabFontSize = "14px",
            tabMinWidth = ns->value("UI/TabMinWidth", "200px").toString(),
            tabMaxWidth = ns->value("UI/TabMaxWidth", "200px").toString(),
            tabMinHeight = ns->value("UI/TabMinHeight", "17px").toString(),
            tabMaxHeight = ns->value("UI/TabMaxHeight", "20px").toString(),
            urlMinWidth = ns->value("UI/UrlMinWidth", "200px").toString(),
            urlMaxWidth = ns->value("UI/UrlMaxWidth", "200px").toString(),
            progressTextFg = ns->value("Color/ProgressText/FG", "#FF5000").toString(),
            progressTextBg = ns->value("Color/ProgressText/BG", "#1e3440").toString(),
            progressTextFont = ns->value("Font/ProgressText", "Roboto").toString(),
            progressTextSize = ns->value("Font/ProgressTextSize", "12px").toString(),
            urlTextFont = ns->value("Font/UrlText", "Roboto").toString(),
            urlTextSize = ns->value("Font/UrlTextSize", "14px").toString();
         
                      
    comboBox->setStyleSheet(QString::fromStdString(R"(
        QComboBox:focus {
            background-color: %1;
            color: %2;
        }
        QComboBox {
            background-color: %3;
            border: none;
            border-radius: 0px;
            color: %4;
            font-size: %5;
            font-family: %6;
        }
        QComboBox[text=\"\"]{
            color: %7;
            background-color: %8;
        }
    )").arg(urlBoxBg, urlBoxFg, urlBoxUnfocusedBg, urlBoxUnfocusedFg,urlBoxFontSize, urlBoxFont,
                urlPlaceHolderFg, urlPlaceHolderBg));

    tabWidget->setStyleSheet(QString::fromStdString(R"(
        QTabWidget::pane {
        }

        QTabWidget::tab-bar:top {
        }

        QTabWidget::tab-bar:bottom {
            bottom: 1px;
        }

        QTabWidget::tab-bar:left {
            right: 1px;
        }

        QTabWidget::tab-bar:right {
            left: 1px;
        }

        QTabBar::tab {
          padding: 5px;
          min-width: %11;
          max-width: %12;
          min-height: %13;
          max-height: %14;
        }

        QTabBar::tab:selected {
            background-color: %1;
            color: %2;
            font-family: %3;
            font-size: %4;
        }

        QTabBar::tab:!selected {
            background-color: %5;
            color: %6;
            font-family: %7;
            font-size: %8;
        }

        QTabBar::tab:!selected:hover {
            background-color: %9;
            color: %10;
        }

    )").arg(tabBgSelected, tabFgSelected, tabSelectedFont, tabSelectedSize, tabBgUnselected,
    tabFgUnselected, tabUnSelectedFont, tabUnSelectedSize, tabBgHover, tabFgHover, tabMinWidth, tabMaxWidth,
        tabMinHeight, tabMaxHeight));

    urlText->setStyleSheet(QString::fromStdString(R"(
            background-color: %1;
            color: %2;
            font-family: %3;
            font-size: %4;
            min-width: %5;
            max-width: %6;
    )").arg(urlTextBg, urlTextFg, urlTextFont, urlTextSize, urlMinWidth, urlMaxWidth));
    modeText->setStyleSheet(QString::fromStdString(R"(
            background-color: %1;
            color: %2;
    )").arg(cmdBoxBg, urlBoxFg));

    progressText->setStyleSheet(QString::fromStdString(R"(
            background-color: %1;
            color: %2;
            font-size: %3;
            font-family: %4;
        )").arg(cmdBoxBg, progressTextFg, progressTextSize, progressTextFont));

    cmdBox->setStyleSheet(QString("background-color: %1").arg(cmdBoxBg));
}

void MainWindow::init_ui()
{
    clip = QApplication::clipboard();
    msgBox = new MessageBox();
    msgBox->setContentsMargins(0, 0, 0, 0);
    centralWidget = new QWidget();
    centralWidget->setContentsMargins(0, 0, 0, 0);
    verticalLayout = new QVBoxLayout();
    verticalLayout->setContentsMargins(0, 0, 0, 0);
    completer = new QCompleter(OPTS);
    completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    comboBox = new ComboBox();
    comboBox->setEditable(true);
    comboBox->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    QShortcut *t = new Shortcut("Return", comboBox);
    connect(t, &QShortcut::activated, comboBox, [this] {
        handleUrlInput();
        comboBox->clearFocus();
        if(comboBox->count() > 10)
            comboBox->removeItem(0);
        comboBox->insertItem(comboBox->count(), comboBox->lineEdit()->text());
    });

    cmdBox = new QWidget();
    cmdLayout = new QHBoxLayout();
    cmdLayout->setContentsMargins(0, 0, 0, 0);
    cmdLayout->setSpacing(0);
    tabWidget = new TabWidget();
    tabWidget->setDocumentMode(true);
    tabWidget->setMovable(true);
    tabWidget->setElideMode(Qt::ElideRight);
    tabWidget->tabBar()->setExpanding(true);
    tabWidget->setCurrentIndex(-1);
    progressText = new QLabel();
    modeText = new QLabel();
    urlText = new QLabel();

    cmdLayout->addWidget(comboBox);
    cmdLayout->addWidget(modeText);
    cmdLayout->addWidget(progressText);
    cmdLayout->addWidget(urlText);
    
    cmdBox->setLayout(cmdLayout);

    verticalLayout->addWidget(tabWidget);
    verticalLayout->addWidget(cmdBox);
    verticalLayout->addWidget(msgBox);
    centralWidget->setLayout(verticalLayout);
    this->setCentralWidget(centralWidget);
}
void MainWindow::init_shortcuts()
{
    QShortcut *pageEscape,
              *pageNext,
              *pageBack,
              *pageReload,
              *pageScrollDown,
              *pageScrollUp,
              *pageScrollLeft,
              *pageScrollRight,
              *nextTab,
              *prevTab,
              *scrollBottom,
              *scrollTop,
              *find,
              *addTab,
              *deleteTab,
              *cmdFocus,
              *pageSearchNext,
              *pageSearchPrev,
              *open,
              *openNewTab,
              *hideTabs,
              *hideCmdBox,
              *hideScrollBar,
              *visual,
              *hideAll,
              *yankURL,
              *tab1,
              *tab2,
              *tab3,
              *tab4,
              *tab5,
              *tab6,
              *linkHint;

    find = new Shortcut("/", tabWidget);
    connect(find, &QShortcut::activated, this, [this] {
        comboBox->setEditText("find ");
        comboBox->setFocus();
    });
    yankURL = new Shortcut("Y, L", tabWidget);
    connect(yankURL, &QShortcut::activated, this, [this] {
        clip->setText(currentWebView()->url().toString());
    });

    visual = new Shortcut("Ctrl+V", tabWidget);
    connect(visual, &QShortcut::activated, this, [this] {
        visualMode = true;
        insertMode = normalMode = false;
    });
    linkHint = new Shortcut("f", tabWidget);
    connect(linkHint, &QShortcut::activated, this, [this] {
        QFile ff("/home/neo/Gits/test2.js");
        ff.open(QIODevice::ReadOnly);
        QString cont = ff.readAll();
        ff.close();
        currentWebView()->page()->runJavaScript(cont);
    });
    tab1 = new Shortcut("Alt+1", tabWidget);
    tab2 = new Shortcut("Alt+2", tabWidget);
    tab3 = new Shortcut("Alt+3", tabWidget);
    tab4 = new Shortcut("Alt+4", tabWidget);
    tab5 = new Shortcut("Alt+5", tabWidget);
    tab6 = new Shortcut("Alt+6", tabWidget);
    connect(tab1, &QShortcut::activated, this, [this] {
        tabWidget->setCurrentIndex(0);
    });
    connect(tab2, &QShortcut::activated, this, [this] {
        tabWidget->setCurrentIndex(1);
    });
    connect(tab3, &QShortcut::activated, this, [this] {
        tabWidget->setCurrentIndex(2);
    });
    connect(tab4, &QShortcut::activated, this, [this] {
        tabWidget->setCurrentIndex(3);
    });
    connect(tab5, &QShortcut::activated, this, [this] {
        tabWidget->setCurrentIndex(4);
    });
    connect(tab6, &QShortcut::activated, this, [this] {
        tabWidget->setCurrentIndex(5);
    });

    hideScrollBar = new Shortcut("X,R", tabWidget);
    connect(hideScrollBar, &QShortcut::activated, this, [this] {
        C_hideScrollBar = !C_hideScrollBar;
        settings = currentWebView()->page()->settings();
        settings->setAttribute(QWebEngineSettings::ShowScrollBars, C_hideScrollBar);
    });

    hideTabs = new Shortcut("X,T", this);
    connect(hideTabs, &QShortcut::activated, this, [this] {
        tabWidget->tabBar()->setHidden(!tabWidget->tabBar()->isHidden());
    });
    hideCmdBox = new Shortcut("X,S", this);
    connect(hideCmdBox, &QShortcut::activated, this, [this] {
        cmdBox->setHidden(!cmdBox->isHidden());
    });

    hideAll = new Shortcut("X,X", tabWidget);
    connect(hideAll, &QShortcut::activated, this, [this] {
        cmdBox->setHidden(!cmdBox->isHidden());
        tabWidget->tabBar()->setHidden(!tabWidget->tabBar()->isHidden());
    });
    scrollBottom = new Shortcut("Shift+G", tabWidget);
    connect(scrollBottom, &QShortcut::activated, this, [this] {
        currentWebView()->page()->runJavaScript(QString::fromStdString(R"(
            var scrollingElement = (document.scrollingElement || document.body);
            scrollingElement.scrollTop = scrollingElement.scrollHeight;
        )"));
    });

    scrollTop = new Shortcut("G,G", tabWidget);
    connect(scrollTop, &QShortcut::activated, this, [this] {
        currentWebView()->page()->runJavaScript("\
            window.scrollTo(0,0);"
        );
    });
    open = new Shortcut("O", tabWidget);
    connect(open, &QShortcut::activated, this, [this] {
        comboBox->setEditText("open ");
        comboBox->setFocus();
    });
    openNewTab = new Shortcut("Shift+O", tabWidget);
    connect(openNewTab, &QShortcut::activated, this, [this] {
        comboBox->setEditText("open -t ");
        comboBox->setFocus();
    });
    pageEscape = new Shortcut("Escape", tabWidget);
    connect(pageEscape, &QShortcut::activated, this, [this] {
        currentWebView()->page()->findText("");
        currentWebView()->page()->runJavaScript("document.activeElement.blur()");
        normalMode = true;
        insertMode = false;
        visualMode = false;
    });
    pageNext = new Shortcut("Shift+L", tabWidget);
    connect(pageNext, &QShortcut::activated, this, [this] {
        currentWebView()->forward();
    });
    pageSearchNext = new Shortcut("N", tabWidget);
    connect(pageSearchNext, &QShortcut::activated, this, [this] {
        findNext();
    });
    pageSearchPrev = new Shortcut("Shift+N", tabWidget);
    connect(pageSearchPrev, &QShortcut::activated, this, [this] {
        findPrev();
    });
    pageBack = new Shortcut("Shift+H", tabWidget);
    connect(pageBack, &QShortcut::activated, this, [this] {
        currentWebView()->back();
    });
    pageReload = new Shortcut("R", tabWidget);
    connect(pageReload, &QShortcut::activated, this, [this] {
        currentWebView()->reload();
    });
    pageScrollDown = new Shortcut("J", tabWidget);
    connect(pageScrollDown, &QShortcut::activated, this, [this] {
        currentWebView()->page()->runJavaScript("window.scrollBy(0, 50)");
    });
    pageScrollUp = new Shortcut("K", tabWidget);
    connect(pageScrollUp, &QShortcut::activated, this, [this] {
        currentWebView()->page()->runJavaScript("window.scrollBy(0, -50)");
    });
    pageScrollLeft = new Shortcut("H", tabWidget);
    connect(pageScrollLeft, &QShortcut::activated, this, [this] {
        currentWebView()->page()->runJavaScript("window.scrollBy(-50, 0)");
    });
    pageScrollRight = new Shortcut("L", tabWidget);
    connect(pageScrollRight, &QShortcut::activated, this, [this] {
        currentWebView()->page()->runJavaScript("window.scrollBy(50, 0)");
    });
    nextTab = new Shortcut("Alt+L", tabWidget);
    connect(nextTab, &QShortcut::activated, this, [this] {
        if(tabWidget->currentIndex() != tabWidget->count()-1)
            tabWidget->setCurrentIndex(tabWidget->currentIndex()+1);
        else
            tabWidget->setCurrentIndex(0);
    });
    prevTab = new Shortcut("Alt+H", tabWidget);
    connect(prevTab, &QShortcut::activated, this, [this] {
        if(tabWidget->currentIndex() != 0)
            tabWidget->setCurrentIndex(tabWidget->currentIndex()-1);
        else
            tabWidget->setCurrentIndex(tabWidget->count()-1);
    });
    addTab = new Shortcut("Ctrl+T", tabWidget);
    connect(addTab, &QShortcut::activated, this, [this] {
        addNewTab("");
    });
    deleteTab = new Shortcut("Ctrl+W", tabWidget);
    connect(deleteTab, &QShortcut::activated, this, [this] {
        tabWidget->removeTab(tabWidget->currentIndex());
    });
    cmdFocus = new Shortcut(":", tabWidget);
    connect(cmdFocus, &QShortcut::activated, this, [this] {
        comboBox->setFocus();
        comboBox->lineEdit()->selectAll();
    });
}

QWebEngineView* MainWindow::currentWebView()
{
    return qobject_cast<QWebEngineView *>(tabWidget->currentWidget());
}

void MainWindow::addNewTab(QString url)
{
    WebView *w = new WebView();
    w->setUrl(url);
    int i = tabWidget->addTab(w, w->url().toString());
    tabWidget->setCurrentIndex(i);
    connect(w, &QWebEngineView::urlChanged, this, [this, w=w] {
        MainWindow::urlBarUpdate(w, w->url());
    });
    connect(w, &QWebEngineView::titleChanged, this, [this, w=w] {
        MainWindow::titleUpdate(w, w->title());
    });
    connect(w, &QWebEngineView::loadStarted, this, [this] {
        QString urlProgressFgColor = "#FF2123";
        //urlText->setStyleSheet(QString("color: %1").arg(urlProgressFgColor));
    });
    connect(w, &QWebEngineView::loadProgress, this, [this, w=w](int progress) {
        MainWindow::progress(w, progress);
    });
    connect(w, &QWebEngineView::loadFinished, this, [this, w=w] {
        QString loadFinishUrlFg = "#2EDE00";
        progressText->setText("");
        tabWidget->setTabText(tabWidget->indexOf(w),
                                       QString("%1").arg(w->title()));
        //urlText->setStyleSheet(QString("color: %1").arg(loadFinishUrlFg));
        currentWebView()->page()->runJavaScript(jQuery);
    });
}

void MainWindow::findNext()
{
    QString t = comboBox->lineEdit()->text();
    if(!t.isEmpty())
    {
    QStringList d = t.split(' ');
    if(d.length() == 2)
        find(d[1]);
    }
}

void MainWindow::findPrev()
{
    QString t = comboBox->lineEdit()->text();
    if(!t.isEmpty())
    {
    QStringList d = t.split(' ');
    if(d.length() == 2)
        find(d[1], true);
    }
}

void MainWindow::find(QString text, bool reverse)
{
    if (!reverse)
    {
        currentWebView()->page()->findText(text, QWebEnginePage::FindFlags(), [=](bool found) {
           searchCallback(found);
        });
    }
    else
    {
        currentWebView()->page()->findText(text, QWebEnginePage::FindBackward, [=](bool found) {
           searchCallback(found);
        });
    }
}

void MainWindow::searchCallback(bool found)
{
    if(found)
        qDebug() << "DD";
}
void MainWindow::urlBarUpdate(QWebEngineView* w, QUrl url)
{
    if(w == currentWebView())
    {
        urlText->setText(url.toString());
    }
}

void MainWindow::titleUpdate(QWebEngineView* w, QString title)
{
    tabWidget->setTabText(tabWidget->indexOf(w), title);
}

void MainWindow::progress(QWebEngineView *w, int progress)
{
    if(w == currentWebView())
    {
         progressText->setText(QString("%1%").arg(progress));
    }
     tabWidget->setTabText(tabWidget->indexOf(w),
                                       QString("[%2%] %1").arg(w->title(),
                                                       QString::number(progress)));
}
void MainWindow::handleUrlInput()
{
    QString CMD = comboBox->currentText();
    HIST << CMD;
    QStringList cmds = CMD.split(' ');
    if(cmds.length() != 0)
    {
        if(cmds[0].startsWith("file:///"))
        {
            //TODO
            currentWebView()->setUrl(cmds[0].split("file:///")[1]);
        }
        else if(!cmds[0].compare("open"))
        {
            if(!cmds[1].compare("-t"))
            {
                if(cmds.length() > 3)
                    for(uint32_t i=2; i<cmds.length(); i++)
                        {
                            addNewTab(correctedURL(cmds[i]));
                        }
                else
                    addNewTab(correctedURL(cmds[2]));
            }
            if(cmds.length() > 2)
            {
                currentWebView()->setUrl(correctedURL(cmds[1]));
                for(uint32_t i=2; i<cmds.length(); i++)
                    addNewTab(correctedURL(cmds[i]));
            }
            else
                currentWebView()->setUrl(correctedURL(cmds[1]));
        }
        else if(!cmds[0].compare("reload"))
            currentWebView()->reload();

        else if(!cmds[0].compare("stop"))
            currentWebView()->stop();

        else if(!cmds[0].compare("forward"))
            currentWebView()->forward();

        else if(!cmds[0].compare("back"))
            currentWebView()->back();

        else if(!cmds[0].compare("find"))
        {
            find(cmds[1]);
        }
        else if(!cmds[0].compare("quit") or !cmds[0].compare("q"))
            this->close();
        
        else if(!cmds[0].compare("reload-config"))
        {
            delete ns;
            init_settings();
            init_stylesheets();
        }
        else
        {
            msgBox->showMessage("Err", "Invalid Command", 2000);
        }
    }
}
QString MainWindow::correctedURL(QString url)
{
    if(!url.contains("https") || !url.contains("http") && !url.startsWith("file:///"))
    {
        url = "https://" + url;
        return url;
    }
    return "";
}

