#include "pch.hpp"
#include "ComboBox.hpp"
class MainWindow : public QMainWindow
{
    public:
        bool C_showTab = true,
             C_showStatus = true,
             C_hideStatusAfterCompletion = true,
             C_showTitleIcon = false,
             C_showClearButton = true,
             C_hideScrollBar = false;

        bool visualMode = false, insertMode = false,
             normalMode = true;

        QString AppConfigDir = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
        QString AppSettingFile = AppConfigDir + "/settings.ini";
        QFile f;
        QSettings *ns;
        QClipboard *clip;
        QCompleter *completer;
        QStringList OPTS, HIST;
        QWebEngineSettings *settings;
        QWidget *centralWidget, *cmdBox;
        QVBoxLayout *verticalLayout;
        QHBoxLayout *cmdLayout;
        ComboBox *comboBox;
        QLabel *progressText, *modeText, *urlText;
        MainWindow();
        ~MainWindow() {}
        TabWidget *tabWidget;
        MessageBox *msgBox;
        QWebEngineView* webview;
        QString jQuery;
        void find(QString, bool reverse=false);
        void searchCallback(bool);
        void findNext();
        void findPrev();
        void init_shortcuts();
        void init_stylesheets();
        void init_ui();
        void init_settings();
        void addNewTab(QString);
        void urlBarUpdate(QWebEngineView *, QUrl);
        void titleUpdate(QWebEngineView*, const QString);
        void handleUrlInput();
        void urlBoxReturnPress();
        void progress(QWebEngineView *, int);
        QString correctedURL(QString url);
        QWebEngineView* currentWebView();
};
