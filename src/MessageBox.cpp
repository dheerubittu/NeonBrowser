#include "MessageBox.hpp"

MessageBox::MessageBox()
{
    label = new QLabel();
    hl = new QHBoxLayout();
    t = new QTimer();
    this->setLayout(hl);
    hl->addWidget(label);
    connect(t, &QTimer::timeout, this, [this] {
        this->hide();
    });
    this->hide();
}

void MessageBox::showMessage(const QString type, const QString msg, const uint duration)
{
    qDebug() << "D";
    label->setText(msg);
    this->show();

    if(!type.compare("Err"))
    {
        this->setStyleSheet(QString("background-color: %1; color: %2").arg(
                                ErrBg, ErrFg));
    }
    t->start(duration);
}
