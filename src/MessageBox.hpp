class MessageBox : public QWidget {
public:
    MessageBox();
    ~MessageBox() {}
    QLabel *label;
    QHBoxLayout *hl;
    QTimer *t;
    QString ErrFg="#FFFFFF", ErrBg="#FF5000";
    void showMessage(const QString type="Info", const QString msg="", const uint duration=1000);
};
