//NeoSettings.setValue("KEY", "VALUE");
void MainWindow::init_settings()
{
    ns = new QSettings(AppSettingFile, QSettings::IniFormat);
    if(!QDir(AppConfigDir).exists())
    {
        QDir().mkdir(AppConfigDir);

        ns->beginGroup("General");

        ns->setValue("userAgent", "");
        
        ns->endGroup();

        ns->beginGroup("UI");

        ns->setValue("ShowTabs", true);
        ns->setValue("ShowStatusBar", true);
        ns->setValue("ShowProgress", true);
        ns->setValue("ShowURL", true);
        ns->setValue("ShowLinkHover", true);
        ns->setValue("ShowCommandBoxPlaceHolderText", true);
        ns->setValue("CommandBoxPlaceHolderText", "CMD");
        ns->setValue("TabAutoHide", false);
        ns->setValue("TabAutoHideSingleTab", false);
        ns->setValue("ShowMessageBox", true);
        ns->setValue("ShowCompletion", true);
        ns->setValue("CommandBarHeight", "14px");
        ns->setValue("CommandBarPadding", "2px");
        ns->setValue("TabMinHeight", "20px");
        ns->setValue("TabMinHeight", "20px");
        ns->setValue("TabMinWidth", "200px");
        ns->setValue("TabMinWidth", "200px");
        ns->setValue("UrlMinWidth", "200px");
        ns->setValue("UrlMaxWidth", "200px");
        ns->setValue("ShowTabNumber", true);
        ns->endGroup();

        ns->beginGroup("Color");

        ns->setValue("ErrorBox/BG", "#FF5000");
        ns->setValue("ErrorBox/FG", "#D4D4D4");
        ns->setValue("InfoBox/BG", "#D4D4D4");
        ns->setValue("InfoBox/FG", "#000000");

        ns->setValue("TabSelected/BG", "#1e3440");
        ns->setValue("TabSelected/FG", "#D4D4D4");
        ns->setValue("TabUnSelected/BG", "#262626");
        ns->setValue("TabUnSelected/FG", "#666666");
        ns->setValue("TabHover/FG", "#333333");
        ns->setValue("TabHover/BG", "#D4D4D4");
        ns->setValue("CommandBoxFocused/BG", "#1e3440");
        ns->setValue("CommandBoxFocused/FG", "#D4D4D4");
        ns->setValue("CommandBoxUnFocused/BG", "#1e3440");
        ns->setValue("CommandBoxUnFocused/FG", "#444444");
        ns->setValue("CommandBoxPlaceHolder/FG", "#444444");
        ns->setValue("CommandBoxPlaceHolder/BG", "#444444");
        ns->setValue("InsertMode/FG", "#444444");
        ns->setValue("InsertMode/BG", "#1e3440");
        ns->setValue("VisualMode/FG", "#444444");
        ns->setValue("VisualMode/BG", "#1e3440");
        ns->setValue("NormalMode/FG", "#444444");
        ns->setValue("NormalMode/BG", "#1e3440");
        ns->setValue("CommandBoxUnFocused/FG", "#444444");
        ns->setValue("CommandBoxUnFocused/BG", "#444444");
        ns->setValue("ProgressText/FG", "#FF5000");
        ns->setValue("ProgressText/BG", "#1e3440");
        ns->setValue("URLText/FG", "#FF5000");
        ns->setValue("URLText/BG", "#1e3440");
        ns->setValue("URLFinished/FG", "#");
        ns->setValue("URLFinished/BG", "#");
        ns->setValue("URLProgress/FG", "#");
        ns->setValue("URLProgress/BG", "#");
        ns->setValue("URLStarting/FG", "#");
        ns->setValue("URLStarting/BG", "#");
        ns->setValue("URLHover/FG", "#");
        ns->setValue("URLHover/BG", "#");

        ns->endGroup();

        ns->beginGroup("Font");
        ns->setValue("CommandBox", "Cascadia Code");
        ns->setValue("CommandBoxSize", "12px");
        ns->setValue("CommandBoxFocused", "Roboto");
        ns->setValue("CommandBoxFocusedtSize", "25px");
        ns->setValue("TabSelected", "Roboto");
        ns->setValue("TabSelectedSize", "15px");
        ns->setValue("TabUnSelected", "Cantarell");
        ns->setValue("TabUnSelectedSize", "13px");
        ns->setValue("ProgressText", "Cascadia Code");
        ns->setValue("ProgressTextSize", "20px");
        ns->setValue("UrlText", "Roboto");
        ns->setValue("UrlTextSize", "13px");
        
        ns->endGroup();

        ns->beginGroup("Shortcut");
        ns->setValue("Reload", "r");
        ns->setValue("Open", "o");
        ns->setValue("OpenNewTab", "Shift+o");
        ns->setValue("ToggleTabs", "x+t");
        ns->setValue("ToggleStatusBar", "x+s");
        ns->setValue("ToggleUI", "x+x");
        ns->setValue("yankURL", "y+l");
        ns->setValue("Tab1", "Alt+1");
        ns->setValue("Tab2", "Alt+2");
        ns->setValue("Tab3", "Alt+3");
        ns->setValue("Tab4", "Alt+4");
        ns->setValue("Tab5", "Alt+5");
        ns->setValue("Tab6", "Alt+6");
        ns->setValue("CloseTab", "Ctrl+w");
        ns->setValue("NewTab", "Ctrl+t");
        ns->setValue("ScrollLeft", "h");
        ns->setValue("ScrollDown", "j");
        ns->setValue("ScrollUp", "k");
        ns->setValue("ScrollRight", "l");
        ns->setValue("PageEscape", "Escape");
        ns->setValue("PageForward", "Shift+l");
        ns->setValue("PageBack", "Shift+h");
        ns->setValue("TabNext", "Alt+l");
        ns->setValue("TabPrev", "Alt+h");
        ns->setValue("ScrollBottom", "Shift+G");
        ns->setValue("ScrollTop", "G+G");
        ns->setValue("Command", ":");
        ns->setValue("SearchNext", "n");
        ns->setValue("SearchPrev", "Shift+n");
        ns->endGroup();

        ns->beginGroup("Profile");
        ns->setValue("StartPage", "https://www.google.com");
        ns->endGroup();

        ns->beginGroup("Search");
        ns->setValue("DefaultSearch", "https://www.google.com/search?q={}&");
        ns->endGroup();

        ns->beginGroup("CustomSearch");
        ns->setValue("G", "https://www.google.com/search?q={}&");
        ns->setValue("DD", "https://www.duckduckgo.org/search?q={}&");
        ns->setValue("AW", "{}");
        ns->endGroup();

        ns->sync();
    }
    else
    {
    }
}
