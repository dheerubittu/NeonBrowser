class TabWidget : public QTabWidget
{
    public:
        TabWidget() {}
        ~TabWidget() {}
protected:
        void mousePressEvent(QMouseEvent *e)
        {
            if(e->button() == Qt::MiddleButton)
            {
                removeTab(tabBar()->tabAt(e->pos()));
            }
            else
            {}
        }
};
