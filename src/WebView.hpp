class WebView : public QWebEngineView
{
    public:
        const QString UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36";
        WebView()
        {
            this->setFocusPolicy(Qt::NoFocus);
            this->page()->profile()->setHttpUserAgent(UserAgent);
        }
        ~WebView() {}
protected:
        void focusInEvent(QFocusEvent *e)
        {
            if(e->type() == QFocusEvent::FocusIn)
                qDebug() << "DD";
        }
};
